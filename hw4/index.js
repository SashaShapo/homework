// <body>
// <div>
// <span>Enter a data please:</span><br/>
// <input type='text' id='name'>
//     <input type='text' id='surname'>
//     </div>
//     <div>
//     <button id='ok'>OK</button>
//     <button id='cancel'>Cancel</button>
//     </div>
//     </body>

var body = {
    tagName: 'body',
    paired: true,
    attrs: null,
    nestedTags: [
        {tagName: 'div',
            paired: true,
            attrs: null,
            nestedTags: [
                {tagName: 'span',
                    paired: true,
                    attrs: null,
                    content: 'Enter a data please:'
                },
                {tagName: 'br',
                    paired:false
                },
                {tagName: 'input',
                    paired: false,
                    attrs: {
                        type: 'text',
                        id: 'name'
                    },
                },
                {tagName: 'input',
                    paired: false,
                    attrs: {
                        type: 'text',
                        id: 'surname'
                    },
                }
            ]

        },
        {tagName: 'div',
            paired: true,
            attrs: null,
            nestedTags: [
                {tagName: 'button',
                    paired: true,
                    attrs: {
                        id: 'ok',
                    },
                    content: 'OK'
                },
                {tagName: 'button',
                    paired: true,
                    attrs: {
                        id: 'cancel',
                    },
                    content: 'Cancel'
                }
            ]
        }
    ],
    content: null,
}

